import Vue from 'vue'
import Vuetify from 'vuetify'
import { InlineSvgPlugin } from 'vue-inline-svg'
import 'vuetify/dist/vuetify.min.css'

// Register global components

Vue.use(Vuetify, { options: { customProperties: true }})
Vue.use(InlineSvgPlugin)

const theme = {
  themes: {
    light: {
      primary: '#4DA100',
      accent: '#83D300',
      secondary: '#D3D017',
      success: '#4DA100',
      info: '#2196F3',
      warning: '#FB8C00',
      error: '#EE5252'
    }
  }
}

export default new Vuetify({ theme })