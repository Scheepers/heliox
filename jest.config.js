module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  testMatch: ['**/components/**/*.test.js'],
  transformIgnorePatterns: ["/node_modules/(?!vue-tel-input-vuetify)"],
  moduleDirectories: [
    "node_modules",
    "src/components"
  ],
  setupFiles: ["<rootDir>/src/components/test.setup.js"]
}