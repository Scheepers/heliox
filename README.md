# Heliox
> Helium front end biolerplate

## Cloning from repository
Heliox depends on the Heliox components, so remember to include the
```
--recurse-submodules
```
flag when cloning.

## Project setup
```
npm install
```

### Compile and hot-reload for development
```
npm run serve
```

### Compile and minify for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Generate API documentation
```
npm run docs
```
